program tester
  implicit none

  real(8),dimension(:),allocatable :: x, y
  integer :: i, ios
  character(80) :: inputline

  i = 0
  
  open(unit=10,file='x_values.txt')
    do
      read(10,'(a)',IOSTAT=ios) inputline
      if (ios /= 0) then
        exit
      else
        i = i+1
      endif
    enddo
  close(10)

  allocate( x(i) , y(i) )

  i=1

  open(unit=10,file='x_values.txt')
    do
      read(10,'(a)',IOSTAT=ios) inputline
      if (ios /= 0) then
        exit
      else
        inputline = adjustl(trim(inputline))
        read(inputline,*) x(i)
      endif
      i=i+1
    enddo
  close(10)

  do i=1,size(x)
    y(i) = x(i)**2
  enddo

  open(unit=10,file='y_values.txt')
    do i=1,size(x)
      write(10,*) y(i)
    enddo
  close(10)

  deallocate(x,y)

end
