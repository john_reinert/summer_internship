#
#===============================================
# Written by John D. Reinert
#===============================================

package require PWI_Glyph 2.4

pw::Application setUndoMaximumLevels 5
pw::Application reset
pw::Application markUndoLevel {Journal Reset}

pw::Application clearModified



proc main { } {

    set z 0
    set r 3
    set n 4
    set Dim_i 100
    set Dim_j 200
    set EndSpacing 1.0e-6
    #
    set offset -0.3

    makeCircle [list -$r 0 $z] [list 0 $r $z] [list 0 0 0] "conn1"
    makeConnector [list -$r 0 $z] [list [expr {-$r * $n}] 0 $z] "conn2"
    makeCircle [list [expr {-$r * $n}] 0 $z] [list 0 [expr {$r * $n}] $z] [list 0 0 0] "conn3"
    makeConnector [list 0 [expr {$r * $n}] $z] [list 0 $r $z] "conn4"

    createDimension "conn1" $Dim_i
    createDimension "conn2" $Dim_j
    createDimension "conn3" $Dim_i
    createDimension "conn4" $Dim_j

    createFace "conn1" "conn2" "conn3" "conn4" 
    createEndSpacing $EndSpacing "conn2" "conn4"

    # Make connector for rotation
    # Note:
    #        offset: The rotation vertex offset from the face.
    makeConnector [list 0 $offset $z] [list [expr {-$r * $n}] $offset $z] "conn5"
}

proc makeConnector {pt1 pt2 str} {
    set _TMP(mode_1) [pw::Application begin Create]
    set _TMP(PW_1) [pw::SegmentSpline create]
    $_TMP(PW_1) addPoint "$pt1"
    $_TMP(PW_1) addPoint "$pt2"
    set _TMP(con_1) [pw::Connector create]
    $_TMP(con_1) addSegment $_TMP(PW_1)
    unset _TMP(PW_1)
    $_TMP(con_1) calculateDimension
    $_TMP(mode_1) end
    unset _TMP(mode_1)
    pw::Application markUndoLevel {Create 2 Point Connector}

    set _CN(1) [pw::GridEntity getByName "con-1"]
    $_CN(1) setName "$str"
    pw::Application markUndoLevel {Modify Entity Name}
    unset _CN(1)
}

proc makeCircle {pt1 pt2 pt3 str} {
    set _TMP(mode_1) [pw::Application begin Create]
    set _TMP(PW_1) [pw::SegmentCircle create]
    $_TMP(PW_1) addPoint "$pt1"
    $_TMP(PW_1) addPoint "$pt2"
    $_TMP(PW_1) setCenterPoint "$pt3" {0 0 1}
    set _TMP(con_1) [pw::Connector create]
    $_TMP(con_1) addSegment $_TMP(PW_1)
    $_TMP(con_1) calculateDimension
    unset _TMP(PW_1)
    $_TMP(mode_1) end
    unset _TMP(mode_1)
    pw::Application markUndoLevel {Create Connector}

    set _CN(1) [pw::GridEntity getByName "con-1"]
    $_CN(1) setName "$str"
    pw::Application markUndoLevel {Modify Entity Name}
    unset _CN(1)
}

proc createDimension {str1 num1} {
    set _CN(1) [pw::GridEntity getByName "$str1"]
    set _TMP(PW_1) [pw::Collection create]
    $_TMP(PW_1) set [list $_CN(1)]
    $_TMP(PW_1) do setDimension "$num1"
    $_TMP(PW_1) delete
    unset _TMP(PW_1)
    pw::Application markUndoLevel {Dimension}
}

proc createFace {str1 str2 str3 str4} {
    set _CN(1) [pw::GridEntity getByName "$str1"]
    set _CN(2) [pw::GridEntity getByName "$str2"]
    set _CN(3) [pw::GridEntity getByName "$str3"]
    set _CN(4) [pw::GridEntity getByName "$str4"]
    set _TMP(PW_1) [pw::DomainStructured createFromConnectors -reject _TMP(unusedCons) -solid [list $_CN(1) $_CN(2) $_CN(3) $_CN(4)]]
    unset _TMP(unusedCons)
    unset _TMP(PW_1)
    pw::Application markUndoLevel {Assemble Domains}
}

proc createEndSpacing {num1 str1 str2} {
    set _CN(1) [pw::GridEntity getByName $str2]
    set _CN(2) [pw::GridEntity getByName $str1]
    set _TMP(mode_1) [pw::Application begin Modify [list $_CN(2) $_CN(1)]]
    set _TMP(PW_1) [$_CN(1) getDistribution 1]
    $_TMP(PW_1) setEndSpacing $num1
    unset _TMP(PW_1)
    set _TMP(PW_2) [$_CN(2) getDistribution 1]
    $_TMP(PW_2) setBeginSpacing $num1
    unset _TMP(PW_2)
    $_TMP(mode_1) end
    unset _TMP(mode_1)
    pw::Application markUndoLevel {Change Spacings}
}

main

